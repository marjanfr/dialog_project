from django.contrib import admin
from test.models import CareCoordinators_test,Patients_test
from JpegImagePlugin import APP


class CareCoordinatorsAdmin_test(admin.ModelAdmin):
    list_display = ('firstName','lastName')
    search_fields =('firstName','lastName')
    exclude = ('key','lastLoginDate')
    
class ClientAdmin(admin.ModelAdmin):
    list_display = ('firstName','lastName','nickname', 'email')
    search_fields =('firstName','lastName','nickname', 'email')
    



admin.site.register(CareCoordinators_test, CareCoordinatorsAdmin_test)
admin.site.register(Patients_test, ClientAdmin)
