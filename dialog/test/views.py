from django import http
from django.utils import simplejson as json
from django.utils.timezone import datetime, utc

#from django.contrib.auth.hashers import check_password
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
from uuid import uuid4

#from django.core.context_processors import request
from test.models import CareCoordinators_test
from questionnaire.models import Answers, ActionItem
#from httplib import HTTP
#from django.core import serializers
#from django.contrib.auth import authenticate
#from dialog.base import *
from dialog.MySerializer import MySerializer, AppointmentSerializer
from dialog.MyStr import clean_json, get_dic_output
from dialog.Error import json_response_message
from dialog.dateHelper import reformat_date

#from questionnaire.models import Answers, ActionItem
#from django.views.decorators.http import require_http_methods, require_POST
#from authentication.views import roleAuthentication, get_user_detail 
from authentication.views import check_user_key,cc_athentication
#from authentication.models import Users
import ast


@csrf_exempt
def test_get_client(request):
    if request.method == 'POST':
        rawUsername = int(request.POST.get('cid', ''))
        
    elif request.method == 'GET':       
        rawUsername = int(request.GET.get('cid', ''))
        
    cc =    CareCoordinators_test.objects.filter(id=rawUsername)  
    mySerialiser = MySerializer()
        #mySerialiser.type = methodType
        #mySerialiser.ms = 'True'
    data = mySerialiser.serialize(cc)
        
   # data = serializers.serialize("json", lstClients, fields=('tel',))
    #return: "[{\"tel\": \"00443335555\", \"cID\": \"1\", \"nickname\": \"mns\", \"id\": 5, \"address\": \"136, willow tree lane\", \"carers\": [1], \"email\": \"mee@mddd.com\"}]"
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1','200',data))), mimetype='application/javascript')
