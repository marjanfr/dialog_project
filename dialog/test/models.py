import binascii


from django.db import models
from django.db.models.fields.related import ManyToManyField
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.utils.timezone import datetime, utc
#from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_init
from django.conf import settings
from django.db.models.signals import post_save
from dialog.enc import decrypt
#from authentication.models import Users
from django.utils.datetime_safe import datetime
from dialog.SimpleDurationField import SimpleDurationField
from dialog.ExtFileField import FilterdFileField
from dialog.MyFields import TimeFieldWithHelpText
from dialog.encField import AESEncryptedCharField
#from dialog.modelHelper import  decrypt_fields
import os

from ubuntu_sso.gtk.gui import HELP_TEXT_COLOR
from dialog.modelHelper import encModel
from dialog import modelHelper



    
class CareCoordinators_test(modelHelper.encModel): 
   #ccId = models.IntegerField()
     #objects = CareCoordinatorsManager()
    firstName = AESEncryptedCharField(max_length=200) 
    lastName = AESEncryptedCharField(max_length=200)
    username = AESEncryptedCharField(max_length=64,unique=True,help_text="This username will be used by care coordinator to login to the Dialog app")
    password = models.CharField(max_length=64)
    #user = models.ForeignKey(Users, help_text="This username will be used by care coordinator to login to the Dialog app")
    key = models.CharField(max_length=200,null=True, blank=True)   
    lastLoginDate = models.DateTimeField(verbose_name='last login',null=True, blank=True)
    def natural_key(self):
        return (self.id, self.firstName, self.lastName)
    def save(self, *args, **kwargs): 
        if self.pk is None:      
            self.password = make_password(self.password,salt=self.username)   
        elif self.password.find('sha1') == -1:
            self.password = make_password(self.password,salt=self.username)       
        #self.firstName += 'test'
        super(CareCoordinators_test, self).save(*args, **kwargs) # Call the "real" save() method.
    '''def get_query_set(self):
        qrySet = super(Carers, self).get_query_set().object.all()
        for e in qrySet:
            e.firstName += 'jan'
        return qrySet'''       

        
    
    def __init__(self, *args, **kwargs):    
        super(CareCoordinators_test, self).__init__(*args, **kwargs)
        self.decrypt_fields()
        prefix = 'enc_'
       
        '''if self.firstName and self.firstName.startswith(prefix):
            self.firstName = decrypt(settings.SECRET_KEY[:32], binascii.a2b_base64(self.firstName[len(prefix):]))''' 
        '''self.lastName = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.lastName)) '''
        '''self.username = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.username)) '''       
    
     
    def __unicode__(self):
        return u'%s %s' % (self.firstName, self.lastName )   
   
    class Meta: 
        db_table = u'dialog_carecoordinators_test'
        verbose_name = 'care coordinator'
        verbose_name_plural = 'care coordinators'
        #app_label = 'Appointments Management'

'''def cc_handler(sender, **kwargs):
  print(sender.firstName)     

post_init.connect(cc_handler, sender=CareCoordinators)'''   
        
'''class Users_key(models.Model):    
    cc = models.ForeignKey(CareCoordinators, verbose_name='carecoordinator')
    key = models.CharField(max_length=200)
    lastLoginDate = models.DateTimeField(verbose_name='last login')
    
    
    def __unicode__(self):
        return u'%s %s' % (self.cc, self.key )   
   
    class Meta: 
        db_table = u'dialog_users_key'
        verbose_name = 'user key'
        verbose_name_plural = 'users key' 
        #app_label = 'Appointments Management'''   
        
         

class Patients_test(modelHelper.encModel):
   # cID = models.CharField(max_length=64, verbose_name='Client ID')
    firstName = AESEncryptedCharField(max_length=300)
    lastName = AESEncryptedCharField(max_length=300)
    nickname = AESEncryptedCharField(max_length=100)
    street = AESEncryptedCharField(max_length=200,verbose_name='street',null=True, blank=True)
    county = AESEncryptedCharField(max_length=200,verbose_name='county',null=True, blank=True,)
    city = AESEncryptedCharField(max_length=100,null=True, blank=True)
    postcode = AESEncryptedCharField(max_length=100,null=True, blank=True)
    country = AESEncryptedCharField(max_length=100,null=True, blank=True)
    username = AESEncryptedCharField(max_length=64,unique=True)
    password = models.CharField(max_length=64)
    #user = models.ForeignKey(Users)
    telHome = AESEncryptedCharField(max_length=50,null=True, blank=True, verbose_name='Tel. (home)')
    telMobile = AESEncryptedCharField(max_length=50,null=True, blank=True, verbose_name='Tel. (mobile)')
    telWork =AESEncryptedCharField(max_length=50,null=True, blank=True, verbose_name='Tel. (work)')
    telOther = AESEncryptedCharField(max_length=50, null=True, blank=True, verbose_name='Tel. (other)')
    email = models.EmailField(max_length=254,null=True, blank=True,)
    carecoordinator = models.ManyToManyField(CareCoordinators_test)    
    ManyToManyField.db_table = 'dialog_carers_patients'
    def natural_key(self):
        return (self.nickname, self.address_line1, self.address_line2, self.address_line3, self.address_city, self.address_postcode, self.telHome, self.telMobile, self.telWork, self.telOther, self.email)   
    class Meta: 
        db_table = u'dialog_client_test'
        verbose_name = 'client'
        verbose_name_plural = 'clients'
        #app_label = 'Appointments Management'
    def __unicode__(self):
        return self.nickname
    def save(self, *args, **kwargs): 
        if self.pk is None:      
            self.password = make_password(self.password,salt=self.username)       
        super(Patients_test, self).save(*args, **kwargs) 
        #self._current['participant']
        '''user = User(username=self.username,first_name=self.firstName,last_name=self.lastName,email='t@t.com',password=self.password,is_staff=False,is_active=True,is_superuser=False,last_login=datetime.now(),)
        user.save()
        userProfile = user.get#UserProfile(user_id=user.get)
        userProfile.is_client = True
        userProfile.is_cc = False
        userProfile.save() '''
    def __init__(self, *args, **kwargs):    
        super(Patients_test, self).__init__(*args, **kwargs)    
        self.decrypt_fields()    
        '''self.nickname = decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(self.nickname)) 
        self.address = decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(self.address))    
        if self.telHome:
            self.telHome = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.telHome))          
        if self.telMobile:
            self.telMobile = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.telMobile))            
        if self.telWork:
            self.telWork = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.telWork))  
        if self.telOther:    
            self.telOther = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.telOther))  '''       

'''def create_user(sender, instance, created, **kwargs):
    if created:
        user = User(username=instance.username,first_name=instance.firstName
                    ,last_name=instance.lastName,email='t@t.com',password=instance.password,is_staff=False,is_active=True,is_superuser=False,last_login=datetime.now(),)
        user.save()
        user.user_permissions = [1]
        user.save()
       

post_save.connect(create_user, sender=Patients)'''              


    

   
'''class UserProfile(models.Model):
    user = models.OneToOneField(User)
    is_client = models.BooleanField()
    is_cc = models.BooleanField()
    class Meta: 
        db_table = u'dialog_userprofile'   
def create_user_profile(sender, instance, created, **kwargs):self._current['participant']
    if created:
        UserProfile.objects.create(user=instance,is_client=True)

post_save.connect(create_user_profile, sender=User)'''


              
    
class Appointments_test(models.Model):
    sync_options = (
        (1,'new'),
        (2,'synchronized')
           ) 
    #id = models.IntegerField(primary_key=True)
    startDate = models.DateTimeField(verbose_name='start date')
    endDate = models.DateTimeField(verbose_name='end date')
    #seenTime = models.DateTimeField(verbose_name='seen time',null=True, blank=True)  
    seenTime = models.TimeField(verbose_name='seen time',null=True, blank=True)    
    duration = SimpleDurationField(verbose_name='duration',null=True, blank=True)
    #duration =  models.DateTimeField(verbose_name='duration',null=True, blank=True)
    comments = models.CharField(max_length=500,null=True, blank=True)
    cc = models.ForeignKey(CareCoordinators_test, verbose_name='carecoordinator')
    patient = models.ForeignKey(Patients_test,verbose_name='client')
    #outcome= models.IntegerField(choices=outcome_options,null=True, blank=True,)
    
    participant = models.CharField(max_length=800,null=True,verbose_name='participants', blank=True
                                   , help_text='Please separate each participant by a semicolon (;).')
    
    #participant = models.ManyToManyField(Participants, verbose_name='participant',null=True, blank=True,)
    #participant = models.ForeignKey(Participants, verbose_name='participant',null=True, blank=True,)
    street = models.CharField(max_length=200,verbose_name='street',null=True, blank=True,)
    county = models.CharField(max_length=200,verbose_name='county',null=True, blank=True,)
    city = models.CharField(max_length=100,null=True, blank=True,)
    postcode = models.CharField(max_length=100,null=True, blank=True,)
    country = models.CharField(max_length=100,null=True, blank=True,)
    syncStatus = models.IntegerField(null=True, blank=True,)
    lastSyncDate = models.DateTimeField(verbose_name='last sync date',null=True, blank=True,)
    class Meta: 
        db_table = u'dialog_appointments_test'     
        verbose_name = 'appointment'
        verbose_name_plural = 'appointments'
        #app_label = 'Appointments Management'

    def __unicode__(self):
        return u'CareCoordinator:%s, Client:%s start date:%s' % (self.cc,self.patient, self.startDate)
    def clean(self):
        from django.core.exceptions import ValidationError
        if self.participant:            
            if self.participant.find(";") == -1:
                raise ValidationError('Please add a semicolon (;) at the end of each participant name.')
        
         #raise ValidationError('Please specify Answer label')
        # do something that validates your data
        
    '''def __init__(self, *args, **kwargs):    
        super(Appointments, self).__init__(*args, **kwargs)
        if self.comments:
            self.comments = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.comments))'''     
    
    
'''class appAnswer(models.Model):
    appointments = models.ForeignKey(Appointments, related_name="appointments")
    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True)
    answered = generic.GenericForeignKey('content_type', 'object_id')
        

class TestGen(models.Model):
    title = models.CharField(max_length=500)
    domain = models.CharField(max_length=500)      
    appointments = generic.GenericRelation(appAnswer)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

          
    def __unicode__(self):
        return u'%s' % (self.title)  
    class Meta: 
        db_table = u'dialog_testgen'         
        verbose_name = 'test'
        verbose_name_plural = 'tests'     '''    
        


    
    