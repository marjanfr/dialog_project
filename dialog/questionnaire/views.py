from django import http
from django.utils import simplejson as json
from django.views.decorators.csrf import csrf_exempt

from questionnaire.models import ActionItem, Answers, Appointments
from dialog.MySerializer import MySerializer
from dialog.MyStr import clean_json, get_dic_output
from authentication.views import check_user_key
from dialog.Error import json_response_message


@csrf_exempt
@check_user_key('400')
def get_actionItems_viaAppointment(request):
    #methodType='apptActions'
    methodType='400'
    if request.method == 'POST':
        appId = int(request.POST.get('appId', -1))
    else:
        return json_response_message("-50",methodType) 
      
    #appId=1
    if appId == -1:
        return json_response_message("-400",methodType)  
    else: 
        lstActions = ActionItem.objects.filter(answer__appointment = appId) 
        mySerialiser = MySerializer()
        #mySerialiser.type = methodType
        #mySerialiser.msg = 'True'
        data = mySerialiser.serialize(lstActions)
        
    #return: "[{\"id\": 1, \"answerId\": 1, \"title\": \"Returns a QuerySet that will"}, {\"id\": 2, \"answerId\": 1, \"title\": \"blog.roseman.org"}]"
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')

@csrf_exempt
@check_user_key('410')
def get_actionItems_viaClient(request):
    #methodType = 'clientActions'
    methodType = '410'
    if request.method == 'POST':
        cId = int(request.POST.get('cId', -1))
    else:
        return json_response_message("-50",methodType) 
      
    #cId=1
    if cId == -1:
        return json_response_message("-210",methodType)  
    else: 
        lstActions = ActionItem.objects.filter(answer__appointment__patient = cId) 
        mySerialiser = MySerializer()
        #mySerialiser.type = methodType
        #mySerialiser.msg = 'True'
        data = mySerialiser.serialize(lstActions)
        
   # data = serializers.serialize("json", lstClients, fields=('tel',))
    #return: "[{\"nickName\": \"mno\", \"id\": 1, \"cID\": \"c1\"}, {\"nickName\": \"h2p\", \"id\": 2, \"cID\": \"c2\"}]"
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')


