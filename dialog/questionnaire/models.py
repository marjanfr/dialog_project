import binascii
from django.db import models
from django.conf import settings
#from dialog.enc import decrypt
from session.models import Patients, Appointments
from dialog import modelHelper
from dialog.encField import AESEncryptedCharField,AESEncryptedIntegerField


class Questions(modelHelper.encModel):
    id = models.IntegerField(primary_key=True)
    order = models.IntegerField()
    title = AESEncryptedCharField(max_length=500)
    domain = AESEncryptedCharField(max_length=500)    
   
    def __init__(self, *args, **kwargs):    
        super(Questions, self).__init__(*args, **kwargs)        
        '''self.title = decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(self.title)) 
        self.domain = decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(self.domain))'''  
       
    def __unicode__(self):
        return u'%s' % (self.title)  
    class Meta: 
        db_table = u'dialog_questions'         
        verbose_name = 'question'
        verbose_name_plural = 'questions' 
        

class RankLabel(modelHelper.encModel):
    id = models.IntegerField(primary_key=True)
    title = AESEncryptedCharField(max_length=200)
    rank = AESEncryptedCharField(max_length=50)
    order = models.IntegerField()
    def __init__(self, *args, **kwargs):    
        super(RankLabel, self).__init__(*args, **kwargs)        
        '''self.title = decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(self.title)) '''    
    def __unicode__(self):
        return u'%s' % (self.title)       
    class Meta: 
        db_table = u'dialog_rankLabel'
        verbose_name = 'Answer rank'
        verbose_name_plural = 'Answer ranks'

    
    
class Answers(modelHelper.encModel):
    needHelpOptions = (('0','unknown'),('2','yes'),('1','no'),)
    question = models.ForeignKey(Questions)
    patient = models.ForeignKey(Patients)
    appointment = models.ForeignKey(Appointments, related_name='answers')
    needHelp = AESEncryptedIntegerField(verbose_name='additional help', choices=needHelpOptions,null=True, blank=True)
    isSelected = models.BooleanField(verbose_name='is selected')
    ranklable = models.ForeignKey(RankLabel,verbose_name='answer label',null=True, blank=True)
    
    def __init__(self, *args, **kwargs):    
        super(Answers, self).__init__(*args, **kwargs)        
        '''if self.rank :
            self.rank = decrypt(settings.SECRET_KEY[:32],
                binascii.a2b_base64(self.rank)) '''
    def __unicode__(self):
        return u'%s, %s' % (self.question, self.appointment)  
    def clean(self):
        from django.core.exceptions import ValidationError
        if self.ranklable and not self.needHelp and self.needHelp <> 0:            
            raise ValidationError('Please choose an option for Additional help.')
        if not self.ranklable and (self.needHelp or self.needHelp == 0):            
            raise ValidationError('Please specify the Answer label.')
    class Meta: 
        db_table = u'dialog_answers'
        verbose_name = 'answer'
        verbose_name_plural = 'answers'

    
class ActionItem(modelHelper.encModel):
    title = AESEncryptedCharField(max_length=500)
    answer = models.ForeignKey(Answers)
    order = AESEncryptedIntegerField()
    def __init__(self, *args, **kwargs):    
        super(ActionItem, self).__init__(*args, **kwargs)    
        self.decrypt_fields()    

    def __unicode__(self):
        return u'%s' % (self.title)  
    class Meta: 
        db_table = u'dialog_actionItem'
        verbose_name = 'action item'
        verbose_name_plural = 'action items'

        


    
    




