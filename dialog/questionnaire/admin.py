from django.contrib import admin
from questionnaire.models import Answers, ActionItem, Questions, RankLabel
from JpegImagePlugin import APP
from django.contrib.auth.models import User
#from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

class AnswersAdmin(admin.ModelAdmin):
    list_display = ('question', 'appointment','ranklable')
    fields=('patient','appointment','question','ranklable','needHelp','isSelected')
    
class ActionItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'answer','order','get_cc','get_client',)
    list_filter = ('answer__appointment__cc',)
    def get_client(self, obj):
        return '%s'%(obj.answer.patient)
    get_client.short_description = 'Client' 
    def get_cc(self, obj):
        return '%s'%(obj.answer.appointment.cc)
    get_cc.short_description = 'CareCoordinator' 
class QuestionsAdmin(admin.ModelAdmin):
    list_display = ('title', 'domain')
    

#admin.site.unregister(User)
#admin.site.unregister(Group)    
admin.site.register(Answers, AnswersAdmin)
admin.site.register(ActionItem, ActionItemAdmin)
admin.site.register(Questions, QuestionsAdmin)
#admin.site.register(RankLabel)
