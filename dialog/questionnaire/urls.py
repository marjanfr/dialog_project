from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('questionnaire.views',
              (r'get/app/', 'get_actionItems_viaAppointment'),
               (r'get/c/', 'get_actionItems_viaClient'),

    # Examples:
    # url(r'^$', 'dialog.views.home', name='home'),
    # url(r'^dialog/', include('dialog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
