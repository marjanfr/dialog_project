from django.shortcuts import render_to_response
#from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import check_password
#from django.core.context_processors import csrf
from django.template import RequestContext
#from django.contrib.redirects.models import Redirect
from django.http import HttpResponseRedirect
from functools import wraps
from django.utils.timezone import datetime, timedelta, utc

#from authentication.models import Users
from session.models import Patients, CareCoordinators
from dialog.Error import json_response_message



def cc_athentication(rawUsername,rawPassword):
    try:
        thisCarer = CareCoordinators.objects.get(username=rawUsername)    
        if thisCarer and thisCarer.password == rawPassword: 
            return thisCarer
    except ObjectDoesNotExist:        
        return None 

'''def roleAuthentication(username,password,encoded=False):
     try:
         usr = Users.objects.get(username=username)
         if (encoded and usr.password == password) or (encoded == False and check_password(password,usr.password)):
            roleId = 1#usr.role  
         else:
             roleId = -1                   
     except ObjectDoesNotExist:
         roleId = -1

     return roleId


def login_user(request):
    state = "Please log in below..."
    return render_to_response('auth.html',{'state':state},context_instance=RequestContext(request))        
           
def get_user_detail(username,role):
    if role == 1:
        return CareCoordinators.objects.get(user__username=username)
    elif role==2:
        return Patients.objects.get(user__username=username)
        
def check_user(request):
    c = {}
    #c.update(csrf(request))
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        role = roleAuthentication(username, password)
        if role == -1:
            state = "Your username and/or password were incorrect."
        elif role == 2:
            state = "You're successfully logged in!"
            thisPatient = get_user_detail(username,2)
            request.session['user_nickname']= thisPatient.nickname
            request.session['user_un']= username
            request.session['user_id'] = thisPatient.id            
        else:
            state = "You're not authorized to access this page"
                     
        ''''''user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "You're successfully logged in!"
            else:
                state = "Your account is not active, please contact the site admin."
        else:
            state = "Your username and/or password were incorrect."''''''

    return HttpResponseRedirect('/capp/')
    #return render_to_response('appointment.html',context_instance=RequestContext(request))
'''    
def check_user_key(methodType):
    def decorator(method):
        def wrapper(request, *args, **kwargs):
            if request.method == 'POST':
                ccId = request.POST.get('ccId', -1)
                postedKey = request.POST.get('k', '')
                if(ccId == -1 or postedKey == ''):                 
                    return json_response_message('ParamDoesntExist',methodType) 
                try: 
                    thisCC = CareCoordinators.objects.get(id=ccId)
                    if thisCC.key == postedKey:
                        if (datetime.utcnow() - thisCC.lastLoginDate) < timedelta(minutes = 20):
                        #if (datetime.utcnow().replace(tzinfo=utc) - thisCC.lastLoginDate) < timedelta(minutes = 20):
                            return method(request, *args, **kwargs)
                        else:                            
                            return json_response_message('-30', methodType) 
                    else:
                        return json_response_message('-33', methodType)                         
                           
                except ObjectDoesNotExist:
                    return json_response_message('-100', methodType)                    
            else:
                return json_response_message('-50',methodType) 
                                     
        return wraps(method)(wrapper)  
    return decorator    