from django.http import Http404
from django.views.generic import list_detail
from django.core.exceptions import ObjectDoesNotExist
from session.models import Appointments, Patients

def appointments_by_client(request):

    # Look up the publisher (and raise a 404 if it can't be found).
    try:
        cId = request.session['user_id'] 
        #clientAppt = Appointments.objects.filter(patient_id=cId)
    except ObjectDoesNotExist:
        raise Http404

    # Use the object_list view for the heavy lifting.
    return list_detail.object_list(
        request,
        queryset = Appointments.objects.filter(patient_id=cId),
        template_name = "appointment.html",
        #template_object_name = "Appointmentg",
        #extra_context = {"publisher" : publisher}
        )