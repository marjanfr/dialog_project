# -*- coding:utf-8 -*-
from django import http
from django.utils import simplejson as json
from django.utils.timezone import datetime, utc

#from django.contrib.auth.hashers import check_password
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError, DatabaseError 
from uuid import uuid4
from django.core.mail import mail_admins


#from django.core.context_processors import request
from session.models import  Patients, Appointments, CareCoordinators,RioDoc
from questionnaire.models import Answers, ActionItem
#from httplib import HTTP
#from django.core import serializers
#from django.contrib.auth import authenticate
#from dialog.base import *
from dialog.MySerializer import MySerializer, AppointmentSerializer
from dialog.MyStr import clean_json, get_dic_output
from dialog.Error import json_response_message
from dialog.dateHelper import reformat_date

#from questionnaire.models import Answers, ActionItem
#from django.views.decorators.http import require_http_methods, require_POST
#from authentication.views import roleAuthentication, get_user_detail 
from authentication.views import check_user_key,cc_athentication
#from authentication.models import Users
import ast
import re
from dialog import dateHelper, settings
from CurImagePlugin import CurImageFile
from django.conf import Settings
from django.db.utils import DatabaseError





@csrf_exempt
def test_user_authentication_hashed(request):
    methodType = "100"



@csrf_exempt
def cc_authentication_hashed(request):
    methodType = "100"
    if request.method == 'POST':
        rawUsername = str(request.POST.get('user', ''))
        rawPassword = str(request.POST.get('pass', ''))
    elif request.method == 'GET':       
        rawUsername = str(request.GET.get('user', ''))
        rawPassword = str(request.GET.get('pass', ''))  
    # rawUsername = 'jan1'
    #rawPassword = '1234
    if rawUsername == '' or rawPassword == '':
        return json_response_message("ParamNotFound", methodType)
        #jsonOutput = [{'msg':'False','err':'null'}]
    else:   
        
        try:
            rawPassword = "sha1" + "$" + rawUsername + "$"  + rawPassword  
            thisCarer = cc_athentication(rawUsername,rawPassword)     
            #thisCarer = CareCoordinators.objects.get(username=rawUsername)    
            if thisCarer:              
                newGuid = uuid4()
                thisCarer.key = newGuid
                thisCarer.lastLoginDate = datetime.utcnow()#.replace(tzinfo=utc)             
                thisCarer.save()
                mySerialiser = MySerializer()
                mySerialiser.setExternalAtt('username', rawUsername)            
                           
                jsonOutput = mySerialiser.serialize({thisCarer}, fields=('firstName','lastName','username','key'))
               
                return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,jsonOutput))), mimetype='application/javascript')
               
            else:
                return json_response_message("-100",methodType)                      
               
          
        except ObjectDoesNotExist:           
            return json_response_message("-100",methodType)

'''@csrf_exempt
def Notused_rolebased_cc_authentication_hashed(request):
    methodType = "100"
    if request.method == 'POST':
        rawUsername = str(request.POST.get('user', ''))
        rawPassword = str(request.POST.get('pass', ''))
    elif request.method == 'GET':       
        rawUsername = str(request.GET.get('user', ''))
        rawPassword = str(request.GET.get('pass', ''))  
    # rawUsername = 'jan1'
    #rawPassword = '1234
    if rawUsername == '' or rawPassword == '':
        return json_response_message("ParamNotFound", methodType)
        #jsonOutput = [{'msg':'False','err':'null'}]
    else:   
        
        #try:
           rawPassword = "sha1" + "$" + rawUsername + "$"  + rawPassword           
           roleId = roleAuthentication(rawUsername, rawPassword, encoded=True)
           if roleId == 1:
               thisCarer = get_user_detail(rawUsername, roleId)
               newGuid = uuid4()
               thisCarer.key = newGuid
               thisCarer.lastLoginDate = datetime.utcnow().replace(tzinfo=utc)             
               thisCarer.save()
               mySerialiser = MySerializer()
               mySerialiser.setExternalAtt('username', rawUsername)
               
               #mySerialiser.msg = 'True'
               #mySerialiser.type = 'auth'                
               jsonOutput = mySerialiser.serialize({thisCarer}, fields=('firstName','lastName','username','key'))
               
               return http.HttpResponse(clean_json(json.dumps(get_dic_output('True',methodType,jsonOutput))), mimetype='application/javascript')
               
           elif roleId == -1:
               return json_response_message("-100",methodType)    
                   
               
           ''''''thisCarer = CareCoordinators.objects.get(username = rawUsername)
           if rawPassword == thisCarer.password:''''''

           ''''''else:
                return json_response_message("wrong pass","auth")    
        except ObjectDoesNotExist:           
            return json_response_message("no user","auth")''''''
          
''' 
      
   
    #{"msg": "True", "output": [{"lastName": "Falah rastegar", "key": "29f7f558-2e6e-4076-854e-d1b2ca18bc3c", "firstName": "Marjan", "id": 1}], "type": "auth", "err": null}  
    #{"msg": "False", "output": null, "type": "auth", "err": "wrong user/pass"}
    #return http.HttpResponse(clean_json(json.dumps(jsonOutput)), mimetype='application/javascript')
@csrf_exempt
@check_user_key('200')
def get_cc_clients(request): 
    #methodType = "lstClients"   
    methodType = "200"   
    if request.method == 'POST':
        ccId = int(request.POST.get('ccId', -1))
    else:
        return json_response_message("-50",methodType) 
       # postedKey = int(request.POST.get('k', ''))
    #else:
        #return json_response_message("ProperMethodNotFound",methodType)   
    #ccId=1
    if ccId == -1:
        return json_response_message("-200",methodType)  
    else: 
        lstClients = Patients.objects.filter(carecoordinator=ccId) 
        mySerialiser = MySerializer()
        #mySerialiser.msg = 'True'
        #mySerialiser.type = methodType
        data = mySerialiser.serialize(lstClients, fields=('nickname','id','firstName','lastName'))
        
    #{"msg": "True", "output": [{"lastName": "Nassabi", "nickname": "mns", "id": 1, "firstName": "Hossein"}], "type": "lstClients", "err": null}
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')

@csrf_exempt
@check_user_key('210')
def get_client_detail(request):
   # methodType = "clientDetail"  
    methodType = "210"      
    if request.method == 'POST':
        cId = int(request.POST.get('cId', -1))
    else:
        return json_response_message("-50",methodType) 
   
    #cId=1
    if cId == -1:
        return json_response_message("-210",methodType)  
    else: 
        clientDetail = Patients.objects.filter(id=cId) 
        mySerialiser = MySerializer()
        #mySerialiser.type = methodType
        #mySerialiser.ms = 'True'
        data = mySerialiser.serialize(clientDetail)
        
   # data = serializers.serialize("json", lstClients, fields=('tel',))
    #return: "[{\"tel\": \"00443335555\", \"cID\": \"1\", \"nickname\": \"mns\", \"id\": 5, \"address\": \"136, willow tree lane\", \"carers\": [1], \"email\": \"mee@mddd.com\"}]"
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')

@csrf_exempt
@check_user_key('300')
def get_appointment(request):
   # methodType = 'ccAppt'
    methodType = '300'
    if request.method == 'POST':
        passedccId = int(request.POST.get('ccId', -1))
    else:
        return json_response_message("-50",methodType) 
    
   
    #passedccId=1
    if passedccId == -1:
        return json_response_message("-200",methodType)  
    else: 
        appointment = Appointments.objects.filter(cc=passedccId) 
        
        
        mySerialiser = AppointmentSerializer()#MySerializer()
        #mySerialiser.type = methodType
        #mySerialiser.msg = 'True'
        data = mySerialiser.serialize(appointment, fields=('id','title','startDate','endDate','patient','syncStatus')) 
    
    #output: "[{\"startDate\": \"2012-08-10T21:10:57Z\", \"endDate\": \"2012-08-15T05:00:00Z\", \"id\": 1, \"title\": \"test\"}]"
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')

import sys
import os
def storeLog(methodType, logStr):   
    #fileName = os.path.join(settings.MEDIA_ROOT,'rioDoc','dialog_log.txt')
    fileName = '/var/www/html/dialog/media/' +'dialog_log.txt'
     
    '''try:           
        f = open(fileName, 'a') 
        f.write(logStr)
    except IOError:
        pass#return json_response_message("-60",methodType) 
    except:
        pass#return json_response_message("-61",methodType) 
        #print "Unexpected error:", sys.exc_info()[0]
    finally:   
        f.close()'''
    return "True"

#from dialog.emailHandler import sendMail
@csrf_exempt
#@check_user_key('310')

def save_appointment(request):
    #methodType = 'svApp'  
    from dialog.enc import encryptInteger
    #newint = encryptInteger(settings.SECRET_INT_KEY, 43)
    #newint2 = encryptInteger(settings.SECRET_INT_KEY, 43)
   
    #sendMail("test body yoohoo","my Subject")
    
    methodType = '310'  
    storemode = 2
    if request.method == 'POST':      
        rawData = str(request.POST.get('a', ''))
        ccId = request.POST.get('ccId', -1)
    elif request.method == 'GET': 
        rawData = str(request.Get['a', ''])
        ccId = request.Get.get['ccId', -1]
        '''try:
          data = "test"#json_data['data']
        except KeyError:
          http.HttpResponseServerError("Malformed data!")'''      
    else:
        return json_response_message("-50",methodType) 
    
    if not rawData:
        return json_response_message("-50",methodType) 
    #.replace('\\"', '\\\"')
    jsonData = rawData.replace("\'", "\\'").replace("\r", "\\r").replace('null','\'\'').replace('\"', '\'')
    #jsonData = rawData.replace("\'", "\\'").replace('null','\'\'').replace('\"', '\'')
    cp_jsonData = rawData
    logStr = '---method type: %s, called at: %s \r' % (methodType,datetime.utcnow()) 
    logStr += '-- requested by this ccId:%s\r' % (ccId) 
    logStr += '-- received data:\r'
    logStr += cp_jsonData + '\r'
    #logStr += '-- replaced newline:\r'
    #logStr += cp_jsonData.replace('\n', '\\mm') + '\r'   
    storeLog(methodType, logStr) 
    #jsonData = "[{'type': 0,'dateStart': 'Mon, 08 Oct 2012 12:00:00  0100','withParticipants': [{'name': 'Minos'},{'name': 'Marjan'}],'appointmentId': 0,'seenTime': 'Sun, 07 Oct 2012 13:35:35  0100','ofCarerWithId': 3,'ofClientWithId': 2,'dateEnd': 'Mon, 08 Oct 2012 13:00:00  0100','comments': 'Patient is 100 in a great health!!!','inLocation': {'country': 'UK','street': 'Clapham road','postcode': 'sw9 0la','city': 'London','county': ''}}]"
     #rawData = "[{"type": 0,"dateStart": "Mon, 08 Oct 2012 12:00:00 +0100","withParticipants": [{"name": "Minos"},{"name": "Marjan"}],"appointmentId": 0,"seenTime": "Sun, 07 Oct 2012 13:35:35 +0100","ofCarerWithId": 3,"ofClientWithId": 2,"dateEnd": "Mon, 08 Oct 2012 13:00:00 +0100","comments": "Patient is in a great health!!!","inLocation": {"country": "UK","street": "Clapham road","postcode": "sw9 0la","city": "London","county": ""}}]"
     #rawData = "[{'msg': 'True', 'output': [{'startDate': '2012-09-17T21:04:15Z', 'endDate': '2012-09-17T21:04:16Z', 'cc': 1, 'seenTime': '1997-01-01 16:04:16', 'comments': '','answer': [{'action': [{'title': 'call him again'}, {'title': 'sport activities'}]}]  }]}]"
     #jsonData = json.loads(rawData)
    #testinput = "[{"type": "marjan\'s book"}]"
    #testinput = testinput.replace("\'", "\\'")
    #testStr = ast.literal_eval(testinput)
    
    try:
        tupleData = ast.literal_eval(jsonData)   
        logStr += 'reading successed\r'
    except ValueError:
        logStr += 'reading input error:valueError\r'        
        storeLog(methodType, logStr) 
        #mail_admins('ValueError in reading data', logStr, fail_silently=False, connection=None, html_message=rawData)
        return json_response_message("-66",methodType,msgBody=logStr,htmlMsg=rawData)
    '''except SyntaxError:
        logStr += 'reading input error:syntaxError%s\r' % SyntaxError        
        storeLog(methodType, logStr) 
        #mail_admins('SyntaxError in reading data', logStr, fail_silently=False, connection=None, html_message=rawData)
        return json_response_message("-64",methodType, msgBody=logStr,htmlMsg=rawData )'''
   
    insertedData = []
    currinputFormat='%Y-%m-%d %H:%M:%S'
    curroutputFormat='%d-%m-%Y %H:%M:%S'
    #appointmentIndex = 0 #this is used to generate the output of the method
     #dicOutput = tupleData[0]
    for dicOutput in tupleData:       
        isNewApp = dicOutput['type'] #if it is the new appointment         
        dicOutput['seenTime'] = datetime.strptime(dicOutput['seenTime'][:-6], '%a, %d %b %Y %H:%M:%S') if dicOutput['seenTime'] else None#Sun, 07 Oct 2012 13:35:35 +0100
        dicOutput['dateStart'] = str(reformat_date(datetime.strptime(dicOutput['dateStart'][:-6], '%a, %d %b %Y %H:%M:%S'),inputFormat=currinputFormat,outputFormat=curroutputFormat))
        dicOutput['dateEnd'] = str(reformat_date(datetime.strptime(dicOutput['dateEnd'][:-6], '%a, %d %b %Y %H:%M:%S'),inputFormat=currinputFormat,outputFormat=curroutputFormat))
        
        lstParticipant_comma_sep = ''
        if dicOutput['withParticipants']:
            lstParticipants = dicOutput['withParticipants'] 
            for eachParticipant in lstParticipants:
                currParticipant = eachParticipant['name']
                lstParticipant_comma_sep += ';' + currParticipant
        #dicParticipants = dicOutput['withParticipants']  
        dicLocation = dicOutput['inLocation']         
        if isNewApp == 0:
            currSyncStatus = 1
        elif isNewApp == 1:
            currSyncStatus = 1
        elif isNewApp == 2:
            currSyncStatus = 3
        else:
            currSyncStatus = 3    
               
        appointment = Appointments(
                                    #id = dicOutput['appointmentId'] if isNewApp <> storemode else None,#if it's a new app, an id will be generated automatically
                                    startDate=dicOutput['dateStart'],
                                    endDate=dicOutput['dateEnd'],
                                    cc_id=dicOutput['ofCarerWithId'],
                                    patient_id = dicOutput['ofClientWithId'],
                                    seenTime=dicOutput['seenTime'],
                                    comments=dicOutput['comments'],
                                    duration=dicOutput['actualDuration'],
                                    outcome_id=dicOutput['withOutcomeWithId'],
                                    participant=lstParticipant_comma_sep[1:],
                                    street=dicLocation['street'] if dicLocation else None, 
                                    county= dicLocation['county'] if dicLocation else None ,
                                    city=dicLocation['city'] if dicLocation else None ,
                                    postcode=dicLocation['postcode'] if dicLocation else None ,
                                    country=dicLocation['country'] if dicLocation else None ,
                                    syncStatus = currSyncStatus,
                                    lastSyncDate = datetime.utcnow()           
                                    )

       # appointment.save()
        try:
            appointment.save()
            logStr += '(1)appointment is saved \r'
            #insertedAppointment = {"id":appointment.id,"startDate":dicOutput['dateStart'],"endDate":dicOutput['dateEnd'] }
            insertedData.append({"syncStatus":currSyncStatus,"type":currSyncStatus,"id":appointment.id,"startDate":dateHelper.reformat_date(dicOutput['dateStart']),"endDate":dateHelper.reformat_date(dicOutput['dateEnd']) })
            #appointmentIndex = appointmentIndex + 1 
        except IntegrityError as ierr:            
            if ierr.message.find("cc_id") > 0:
                logStr += "(1)error in saving appointment:ccIdNotFound"
                storeLog(methodType, logStr) 
                return json_response_message("-200",methodType,msgBody=logStr,htmlMsg=rawData)
            elif ierr.message.find("patient_id") > 0:
                logStr += "(1)error in saving appointment:patientIdNotFound"
                storeLog(methodType, logStr) 
                return json_response_message("-210",methodType,msgBody=logStr,htmlMsg=rawData)
        except DatabaseError as dberr:
            logStr += '(1)error in saving appointment:(dbError)'+dberr.message
            storeLog(methodType, logStr)
            return json_response_message("-62",methodType,msgBody=logStr,htmlMsg=rawData)
        except:
            logStr += '(1)error in saving appointment:(unkownError)'+sys.exc_info()[0]
            storeLog(methodType, logStr) 
            return json_response_message("-60",methodType,msgBody=logStr,htmlMsg=rawData)
            
        

        if isNewApp == storemode and dicOutput['questionnaire']:
            dicQuestionnaire = dicOutput['questionnaire']
            lstAnswers = dicQuestionnaire['withAnswers'] 
            for eachAnswer in lstAnswers:
                questionnaireAnswers = Answers(
                                            question_id = eachAnswer['ofQuestionWithId'],
                                            patient_id = dicOutput['ofClientWithId'],
                                            appointment_id = appointment.id,
                                            needHelp = '0' if not eachAnswer['additional_help'] else eachAnswer['additional_help'],
                                            isSelected = 0 if not eachAnswer['selected'] else eachAnswer['selected'],
                                            ranklable_id = 0 if not eachAnswer['withChosenLabelWithId'] else eachAnswer['withChosenLabelWithId']
                                            ) 
                try:
                    questionnaireAnswers.save()  
                except Exception as e:
                        logStr +='(2)error in saving questionnaire:%s\r' % (e)
                        storeLog(methodType, logStr) 
                        return json_response_message("-60",methodType,msgBody=logStr,htmlMsg=rawData)
                lstActions = eachAnswer['withActionItems']
                for eachAction in lstActions:
                    action = ActionItem(
                                        title = eachAction['title'],
                                        answer_id = questionnaireAnswers.id,
                                        order = eachAction['order']
                                        )
                    try:
                        action.save()
                        logStr += '(2) action(s) has/have been stored successfully \r'
                    except Exception as e:
                        logStr += '(3)error in saving action:%s\r' % (sys.exc_info()[0])
                        storeLog(methodType, logStr) 
                        return json_response_message("-60",methodType,msgBody=logStr,htmlMsg=rawData)
                
         
    logStr += '---- END of method call----'        
    storeLog(methodType, logStr) 
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,insertedData))), mimetype='application/javascript')
    #return json_response_message("1",methodType,isValid="1")
