from django.contrib import admin
from django.contrib.contenttypes import generic
from session.models import CareCoordinators, Patients, Appointments, RioDoc,Participants

from session.forms import ClientForm, CCForm, AppointmentForm
from django.contrib.auth.models import User
#from django.contrib.sites.models import Site
from django.contrib.auth.models import Group
from tty import CC



'''class ViewAdmin(admin.ModelAdmin):

    """
    Custom made change_form template just for viewing purposes
    You need to copy this from /django/contrib/admin/templates/admin/change_form.html
    And then put that in your template folder that is specified in the 
    settings.TEMPLATE_DIR
    """
    change_form_template = 'view_form.html'

    # Remove the delete Admin Action for this Model
    actions = None

    def has_add_permission(self, request):
        u = request.user
        try:
          if u.get_profile().is_client:
              return False          
        except:
            return False          
        
        return True

    def has_delete_permission(self, request, obj=None):
        return False
    

    def save_model(self, request, obj, form, change):
        #Return nothing to make sure user can't update any data
        pass
'''


   


'''class ImageInline(generic.GenericStackedInline):
    model = TestGen'''
    
'''class AppointmentAdmin(admin.ModelAdmin):
   def time_seconds(self, obj):
    return obj.seenTime.strptime("%H:%M:%S")

    list_display = ('id','startDate', 'endDate', 'cc','patient','time_seconds')
    form = AppointmentForm

    #inlines = [ImageInline,]


    
   def has_change_permission(self, request, obj=None):
    has_class_permission = super(AppointmentAdmin, self).has_change_permission(request, obj)
    if not has_class_permission:
        return False
    if obj is not None and not request.user.is_superuser and request.user.id != obj.patient.id:
        return False
    return True 
    
 
    
    def queryset(self, request):
        if request.user.is_superuser:
            return Appointments.objects.all()
        return Appointments.objects.all()
'''
class ClientAdmin(admin.ModelAdmin):
    list_display = ('firstName','lastName','nickname', 'email')
    search_fields =('firstName','lastName','nickname', 'email')
    form = ClientForm
    
class AppointmentAdmin(admin.ModelAdmin):
    form = AppointmentForm
    list_display = ('cc','patient','startDate')
    list_filter = ('cc',)
    search_fields =('cc','patient')
    exclude = ('syncStatus','lastSyncDate')
    fields = ('startDate','endDate','cc', 'patient', 'duration', 'seenTime','outcome'
              ,'comments','participant','street','county', 'city','country','postcode')
    
class CareCoordinatorsAdmin(admin.ModelAdmin):
    list_display = ('firstName','lastName')
    search_fields =('firstName','lastName')
    exclude = ('key','lastLoginDate')
    form = CCForm
    
class RioDocAdmin(admin.ModelAdmin):
    list_display = ('title','patient')
    search_fields =('title','patient')
    #form=RioDocForm
    
    
#admin.site.unregister(User)
admin.site.unregister(Group)
#admin.site.unregister(Site)
admin.site.register(CareCoordinators, CareCoordinatorsAdmin)
admin.site.register(Patients, ClientAdmin)
admin.site.register(Appointments,AppointmentAdmin)
admin.site.register(RioDoc,RioDocAdmin)
#admin.site.register(Participants)


#admin.site.register(TestGen)


