from django.forms import ModelForm, TimeInput, PasswordInput, DateTimeField,DateTimeInput,DateField,SplitDateTimeField
from session.models import Appointments,RioDoc,Patients,CareCoordinators
from dialog.FormField import  MyTimeFormField
from dialog.ExtFileField import ExtFileField
from django.forms.extras.widgets import SelectDateWidget

class AppointmentForm(ModelForm):
    #startDate =  DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    class Meta:
        model = Appointments  
        #model.startDate = SplitDateTimeField()
        '''widgets = {
            #'seenTime' : TimeInput(),
            'startDate':SplitDateTimeField(),
        }'''
       

class ClientForm(ModelForm):
    class Meta:
        model = Patients
        widgets = {
            'password' : PasswordInput(),
        }
        
class CCForm(ModelForm):
    class Meta:
        model = CareCoordinators
        widgets = {
            'password' : PasswordInput(),
        }

'''class RioDocForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RioDocForm, self).__init__(*args, **kwargs)
        #self.fields['file']=ExtFileField(ext_whitelist=(".pdf", ".jpg", ".png",".gif"))
        
    
    class Meta:
        model = RioDoc
'''        