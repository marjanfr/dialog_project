
from django.conf.urls.defaults import patterns, include, url
from session.view_docs import get_client_doc_file,get_client_doc_info
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('session.views',
              (r'get/appointment', 'get_appointment'),
              (r'get/clients', 'get_cc_clients'),
              (r'get/detail', 'get_client_detail'),
              (r'get/appointments', 'get_appointment'),     
              (r'get/doc', get_client_doc_file),     
              (r'get/infodoc', get_client_doc_info),            
            
              
    # Examples:
    # url(r'^$', 'dialog.views.home', name='home'),
    # url(r'^dialog/', include('dialog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
