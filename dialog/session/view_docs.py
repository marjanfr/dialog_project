from django import http
import os, tempfile, zipfile
from django.views.decorators.csrf import csrf_exempt
import ntpath
import json
import mimetypes

from dialog.settings import MEDIA_ROOT
from dialog.MySerializer import MySerializer
from dialog.MyStr import clean_json, get_dic_output
from session.models import RioDoc
from dialog.MultiFileWrapper import FixedFileWrapper
from dialog.Error import json_response_message
from authentication.views import check_user_key


def ValuesQuerySetToDict(vqs):
    return [item for item in vqs]

@csrf_exempt
@check_user_key('700')
def get_client_doc_info(request):
    methodType = '700'
    if request.method == 'POST':  
        passedcId = int(request.POST.get('cId', -1))
    else:
        return json_response_message("-50",methodType) 
    
    if passedcId == -1:
        return json_response_message("-210",methodType)  
    clientDocsInfo = RioDoc.objects.filter(patient=passedcId).values('id','title','desc','file')
    for eachDoc in clientDocsInfo:
        eachDoc['file']= os.path.splitext(eachDoc['file'])[1] 
    #data = clientDocsInfo
    
    #mySerialiser = MySerializer() 
   # mySerialiser.addThisName = "doctype"
    #mySerialiser.addThisVal = os.path.splitext(clientDocsInfo.file)[1] 
    #data = mySerialiser.serialize(clientDocsInfo)
    #data = json.dumps(clientDocsInfo)
    '''data=[]
    data.append({'f':'tt','g':23})  
    data.append({'fy':'tt','g':60}  )'''
    
    data= ValuesQuerySetToDict(clientDocsInfo)

    #return http.HttpResponse(json.dumps(date),mimetype="application/json")   # return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')    



def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

@csrf_exempt
@check_user_key('600')
def get_client_doc_file(request):
    methodType = '600'
    if request.method == 'POST':  
        docId = int(request.POST.get('docId', -1))
    else:
        return json_response_message("-50",methodType) 
        
    #docId = 5
    if docId == -1:
        return json_response_message("-600",methodType)  
    clientFile = RioDoc.objects.get(id=docId)#filter(id=docId).values('file')
    
    try:
        file_path = MEDIA_ROOT + str(clientFile.file)
        fsock = open(file_path,"r")
        #file = fsock.read()
        #fsock = open(file_path,"r").read()
        file_name = os.path.basename(file_path)
        file_size = os.path.getsize(file_path)
        #print "file size is: " + str(file_size)
        mime_type_guess = mimetypes.guess_type(file_name)
        if mime_type_guess is not None:
            response = http.HttpResponse(fsock, mimetype=mime_type_guess[0])
        response['Content-Disposition'] = 'attachment; filename=' + file_name            
    except IOError:
        response = http.HttpResponseNotFound()
    return response
    

#@csrf_exempt
#@check_user_key('600')
'''def get_client_doc_file_zip(request):
    methodType = '600'
    if request.method == 'POST':  
        passedcId = int(request.POST.get('docId', -1))
    else:
        passedcId = int(request.GET.get('docId', -1))
       # return json_response_message("-50",methodType) 
    #passedcId= 1
    clientDocs = RioDoc.objects.filter(patient=passedcId).values('file')
    temp = tempfile.TemporaryFile()
    
    archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)
    index = 0
    for clientFile in clientDocs:
        if clientFile['file']:
            filename = MEDIA_ROOT + clientFile['file']# Select your files here.                           
            s= path_leaf(filename)
            #file,ext = s.split('.')
            archive.write(filename,s)
    archive.close()
    wrapper = FixedFileWrapper(temp)
    response = http.HttpResponse(wrapper, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=doc.zip'
    response['Content-Length'] = temp.tell()
    temp.seek(0)
   # mySerialiser = MySerializer()
    #data = mySerialiser.serialize(clientDoc) 
    return response'''




    

