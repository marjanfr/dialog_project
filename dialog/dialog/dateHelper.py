
import datetime


def reformat_date(date_string, inputFormat='%d-%m-%Y %H:%M', outputFormat='%a, %d %b %Y %H:%M:%S'):
  if isinstance(date_string,datetime.datetime):
        date_string = str(date_string)       
  try:
      return datetime.datetime.strptime(date_string, inputFormat).strftime(outputFormat)
  except ValueError:
      try:
          inputFormat='%d-%m-%Y %H:%M:%S'
          return datetime.datetime.strptime(date_string, inputFormat).strftime(outputFormat)
      except ValueError:
          inputFormat='%Y-%m-%d %H:%M:%S'
          return datetime.datetime.strptime(date_string[:-3], inputFormat).strftime(outputFormat)
      