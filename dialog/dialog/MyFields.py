import binascii
import enc
from django.db import models
from django.conf import settings


    
class AESEncryptedCharField(models.CharField):
    def save_form_data(self, instance, data):
        '''setattr(instance, self.name,
            binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], data)))'''
    '''def value_from_object(self, obj):
        return enc.decrypt(settings.SECRET_KEY[:32],
            binascii.a2b_base64(getattr(obj, self.attname)))'''
        
class AESEncryptedIntegerField(models.IntegerField):
    '''def save_form_data(self, instance, data):
        setattr(instance, self.name,
            binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], data)))'''
    
class TimeFieldWithHelpText(models.TimeField):
    
        def formfield(self, **kwargs):
            # This is a fairly standard way to set up some defaults
            # while letting the caller override them.
            defaults = {"help_text": "Enter time in the format: HH:MM:SS"}
            defaults.update(kwargs)
            return super(models.TimeField, self).formfield(**defaults)
    



        