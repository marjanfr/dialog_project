from django.forms.fields import CharField, TimeField
from django.utils.timezone import datetime, utc


class MyTimeFormField(TimeField):
    def __init__(self, *args, **kwargs):
        #self.max_length = 10
        super(MyTimeFormField, self).__init__(*args, **kwargs)

    def clean(self, value):
        value = super(TimeField, self).clean(value)
        dummyDate = datetime.utcnow().replace(tzinfo=utc)
                
        
        return value