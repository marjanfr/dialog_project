import unittest
from django.test.client import Client

class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_details(self):
        # Issue a GET request.
        #response = self.client.post('/cctt/', {'cid':12,'k':'b534c2da-1df2-44d0-a290-cb7b5fe6952c'})
        #response = self.client.post('/client/get/infodoc/', {'ccId':3,'k':'b534c2da-1df2-44d0-a290-cb7b5fe6952c','cId':2})
       # response = self.client.post('/client/get/doc/', {'ccId':3,'k':'b534c2da-1df2-44d0-a290-cb7b5fe6952c','docId':7})
        response = self.client.post('/action/get/app/', {'ccId':3,'k':'59cd029a-daca-4816-8ad7-be85f668c80d','appId':61})
        #response = self.client.post('/all/viaappointment/', {'ccId':3,'k':'c0f0da20-82c8-49b4-87d8-cb230f8d6833','aptId':61})
       # response = self.client.post('/carer/get/appointments/', {'ccId':3,'k':'e8d38e68-d88d-4926-8d01-6372cda35db1','aptId':24})
        #response = self.client.post('/svapp/', {'ccId':3,'k':'6dc40df6-f33d-4ae5-b8e9-9badd877858e','a':'[{"type": 3,"dateCreated": "Sun, 25 Nov 2012 11:31:53 +0000","dateStart": "Sun, 25 Nov 2012 12:00:00 +0000","appointmentId": 0,"withParticipants": [{"name": "katevas minos"},{"name": "marjan falah"}],"seenTime": "Sun, 25 Nov 2012 11:32:16 +0000","ofCarerWithId": 2,"ofClientWithId": 1,"dateEnd": "Sun, 25 Nov 2012 13:00:00 +0000","comments": "sdncoadsvadf","inLocation": null,"questionnaire": {"withAnswers": [{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 9,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 1,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 8,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 10,"withActionItems": []},{"additional_help": 1,"selected": 1,"withChosenLabelWithId": 6,"ofQuestionWithId": 5,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 6,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 7,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 4,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 3,"withActionItems": []},{"additional_help": null,"selected": null,"withChosenLabelWithId": null,"ofQuestionWithId": 2,"withActionItems": []},{"additional_help": 2,"selected": 1,"withChosenLabelWithId": 5,"ofQuestionWithId": 0,"withActionItems": [{"order": 2,"title": "vdfvdfsdf"},{"order": 1,"title": "c advafsdv df"}]}]},"dateUpdated": "Sun, 25 Nov 2012 11:32:46 +0000","actualDuration": 3660,"withOutcomeWithId": 2}]'})
        #response = self.client.post('/svapp/', {'ccId':1,'a':'[{"type": 0,"dateStart": "Sat, 08 Oct 2012 12:00:00 +0100","withParticipants": [{"name": "Minos"},{"name": "Marjan"}],"appointmentId": 0,"seenTime": null,"ofCarerWithId": 1,"ofClientWithId": 1,"dateEnd": "Mon, 08 Oct 2012 13:00:00 +0100","comments": "Patient is in a great health!!!","withOutcomeWithId": 4,"actualDuration": 3600,"questionnaire": {"withAnswers": [{"additional_help": 2,"selected": 1,"withChosenLabelWithId": 6,"ofQuestionWithId": 3,"withActionItems": [{"order": 1,"title": "mpla mpla mpla"},{"order": 2,"title": "mpla mplma mplmapmda"}]},{"additional_help": 2,"selected": null,"withChosenLabelWithId": 6,"ofQuestionWithId": 9,"withActionItems": []}]},"inLocation": {"country": "UK","street": "Slapham road","postcode": "sw9 0la","city": "London","county": ""}}]'})
        #response = self.client.post('/all/viaclient/', {'ccId':3,'k':'a5a4ccb3-7fb8-4c02-abca-635fdfa23a77','cId':1})
       # response = self.client.post('/client/get/detail/', {'ccId':1,'k':'25d4bc87-0739-4155-bc60-2aef2fc1d74e','cId':1})
        #response = self.client.post('/carer/get/clients/', {'ccId':3,'k':'7d4aa523-f89c-4946-a6a4-be06f73e4bb6','cId':1})
        
        #response = self.client.post('/auth/carer/', {'user':'jan','pass':'93dd1b03fabe1f9a708504155322d441abd3fb6e'})
        print response
        # Check that the response is 200 OK.
        self.failUnlessEqual(response.status_code, 200)
       
        # Check that the rendered context contains 5 customers.
        #self.failUnlessEqual(len(response.context['customers']), 5)