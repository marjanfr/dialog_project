from django.core.servers.basehttp import FileWrapper

class FixedFileWrapper(FileWrapper):
    def __iter__(self):        
        self.filelike.seek(0)
        return self