from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from generalDBSync import get_all_viaClient, get_all_viaAppointment, get_all_viaDate
from session.views import cc_authentication_hashed, save_appointment #test_user_authentication_hashed
from session.models import Appointments
from session.client_view import appointments_by_client

#from session.testview import testSave

admin.autodiscover()


urlpatterns = patterns('',
              (r'^session/', include("session.urls")),
              (r'^auth/', include("session.urls")),
              (r'^auth/carer/', cc_authentication_hashed),
             # (r'^auth/test/',test_user_authentication_hashed),
              (r'^test/', include("session.urls")),
              (r'^client/', include("session.urls")),   
              (r'^carer/', include("session.urls")),                            
              (r'^action/', include("questionnaire.urls")),   
              (r'^all/viaclient/', get_all_viaClient),  
              (r'^all/viadate/', get_all_viaDate),  
              (r'^all/viaappointment/', get_all_viaAppointment),
              #(r'^login/$', 'authentication.views.login_user'),  
             # (r'^login/check/$', 'authentication.views.check_user'),
              (r'^capp/$',appointments_by_client),
              (r'^cctt/','test.views.test_get_client'),
              #(r'^capp/(\w+)/$',appointments_by_client),  
              #(r'^testdata/', 'session.testview.testSave'),
              (r'^svapp/', 'session.views.save_appointment'),
              #(r'^$', include(admin.site.urls)),
              (r'^admin/', include(admin.site.urls)),
              
    # Examples:
    url('^$', 'django.views.generic.simple.redirect_to', {'url':
'/admin/'}, name='home'),
    #url(r'^$', 'dialog.views.home', name='home'),
    # url(r'^dialog/', include('dialog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
