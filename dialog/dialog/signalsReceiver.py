from django.db.models.signals import post_init
from django.dispatch import receiver
from session.models import CareCoordinators


@receiver(post_init, sender=CareCoordinators)
def my_callback(sender, **kwargs):
    print(sender.firstName)


