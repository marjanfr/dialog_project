from django.db import models
from dialog.encField import AESEncryptedBaseField
from dialog.enc import decrypt

from settings import encPrefix, SECRET_KEY
import binascii

from django.utils.timezone import datetime

class encModel(models.Model): 
    class Meta:
        abstract = True
    def decrypt_fields(self):
        prefix = encPrefix
        modelDic = {}
        attlst = self._meta.fields    
        for eachAttr in attlst:
            if isinstance(eachAttr, AESEncryptedBaseField):
                value = getattr(self,eachAttr.name)
                if value:
                    if isinstance(value, datetime):
                        setattr(self, eachAttr.name,decrypt(SECRET_KEY[:32], binascii.a2b_base64(value[len(prefix):])))
                    elif value.startswith(prefix) :
                        setattr(self, eachAttr.name,decrypt(SECRET_KEY[:32], binascii.a2b_base64(value[len(prefix):])))




    
