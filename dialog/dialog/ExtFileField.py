import os

from django import forms
from django.db import  models

class ExtFileField(forms.FileField):
    ''' Same as forms.FileField, but you can specify a file extension whitelist.        
        >>> from django.core.files.uploadedfile import SimpleUploadedFile
        >>>
        >>> t = ExtFileField(ext_whitelist=(".pdf", ".txt"))
        >>>
        >>> t.clean(SimpleUploadedFile('filename.pdf', 'Some File Content'))
        >>> t.clean(SimpleUploadedFile('filename.txt', 'Some File Content'))
        >>>
        >>> t.clean(SimpleUploadedFile('filename.exe', 'Some File Content'))
        Traceback (most recent call last):
        ...
        ValidationError: [u'Not allowed filetype!']
    '''
    
    ext_whitelist = (".pdf", ".jpg", ".png",".gif")
    def __init__(self, *args, **kwargs):
        #ext_whitelist = (".pdf", ".jpg", ".png",".gif")#kwargs.pop("ext_whitelist")
        #ext_whitelist = [i.lower() for i in ext_whitelist]
        self.required = False
        super(ExtFileField, self).__init__(*args, **kwargs)
        

    def clean(self, *args, **kwargs):
        data = super(ExtFileField, self).clean(*args, **kwargs)
        
        if data:
            filename = data.name
            #data.file.content_type
            ext = os.path.splitext(filename)[1]
            ext = ext.lower()
            if ext not in self.ext_whitelist:
                raise forms.ValidationError("Filetype '%s' not allowed for this field" % ext)            
        elif not data and self.required:
            raise forms.ValidationError("Required file not found!")
        return data

class FilterdFileField(models.FileField):
    
        def formfield(self, **kwargs):
            
            defaults = {'form_class':ExtFileField, 'max_length':self.max_length}
        # If a file has been provided previously, then the form doesn't require
        # that a new file is provided this time.
        # The code to mark the form field as not required is used by
        # form_for_instance, but can probably be removed once form_for_instance
        # is gone. ModelForm uses a different method to check for an existing file.
            defaults.update(kwargs)
            return super(FilterdFileField, self).formfield(**defaults)