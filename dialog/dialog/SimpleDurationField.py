from django.db.models.fields import IntegerField
from django.db import models
from django.forms.fields import CharField
from django.forms.util import ValidationError as FormValidationError
from django.core.exceptions import ValidationError
from datetime import datetime,time,timedelta,date

class SimpleDurationFormField(CharField):
    def __init__(self, *args, **kwargs):
        self.max_length = 10
        super(SimpleDurationFormField, self).__init__(*args, **kwargs)

    def clean(self, value):
        value = super(CharField, self).clean(value)
        if value and len(value.split(':')) != 2:
            raise FormValidationError('Data entered must be in format MM:SS')
        return value


class SimpleDurationField(IntegerField):
    __metaclass__ = models.SubfieldBase
    
    '''def __init__(self, *args, **kwargs):
         super(SimpleDurationField, self).__init__(*args, **kwargs)'''
    
    def to_python(self, value):
        if not value:
            return None # timedelta(seconds=0)#
        if isinstance(value, (int, long)):
            tt = timedelta(seconds=value)
            minutes, seconds = divmod(tt.seconds, 60) 
            return "%02d:%02d" % (minutes, seconds)
            
            #return timedelta(seconds=value)
        elif isinstance(value, basestring):
            minutes, seconds = map(int, value.split(':'))
            return timedelta(seconds=(seconds + minutes*60))
        elif not isinstance(value, timedelta):
            raise ValidationError('Unable to convert %s to timedelta.' % value)
        return value

    def get_db_prep_value(self, value,connection,prepared=False):
        if value:
            if isinstance(value, basestring):
                minutes, seconds = map(int, value.split(':'))
                return seconds + minutes * 60
            else: 
                return value.seconds + (86400 * value.days)
        else: 
            return None 
    
    def value_to_string(self, instance):
        timedelta = getattr(instance, self.name)
        if timedelta:  
            return timedelta          
            '''minutes, seconds = divmod(timedelta.seconds, 60)
            return "%02d:%02d" % (minutes, seconds)'''
        return None
    '''def formfield(self, form_class=SimpleDurationFormField, **kwargs):
        defaults = {"help_text": "Enter duration in the format: MM:SS"}
        defaults.update(kwargs)
        return form_class(**defaults)'''
    def formfield(self, **kwargs):
        defaults = {'form_class': SimpleDurationFormField,"help_text": "Enter duration in the format: MM:SS"}
        defaults.update(kwargs)
        return super(SimpleDurationField, self).formfield(**defaults)
    
    



        