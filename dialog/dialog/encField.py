import binascii
import enc
import re
from django.db import models
from django.conf import settings
from django.forms import IntegerField,EmailField, Textarea,DateTimeField,DateField,SplitDateTimeField, CharField
from django.utils.translation import ugettext_lazy as _
from settings import encPrefix
from django.utils.timezone import datetime

from django.utils.encoding import smart_str
from django.core import exceptions
from django.core.exceptions import ValidationError

class AESEncryptedBaseField(models.Field):
    prefix = encPrefix
    '''def save_form_data(self, instance, data):
        setattr(instance, self.name,
            binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], data)))'''
        
    def value_from_object(self, obj):
        value = getattr(obj, self.attname)
        if value and value.startswith(self.prefix):
            return enc.decrypt(settings.SECRET_KEY[:32], binascii.a2b_base64(value[len(self.prefix):]))
        return value
    
       
    def to_python(self, value):
        if value and value.startswith(self.prefix):
            return enc.decrypt(settings.SECRET_KEY[:32],binascii.a2b_base64(self.get_prep_value(value)))
         
        return value
        
    def get_prep_value(self, value):
        if value and not value.startswith(self.prefix):
            encvalue = self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value)) 
            return  encvalue
    '''def get_db_prep_value(self, value,connection,prepared=False):
        if value and not value.startswith(self.prefix):
            encvalue = self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value)) 
            return  encvalue'''
    '''def get_prep_lookup(self, lookup_type, value):
        if value and not value.startswith(self.prefix):
            encvalue = self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value)) 
            return  encvalue'''
            
    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        if value and value.startswith(self.prefix):
            return enc.decrypt(settings.SECRET_KEY[:32], binascii.a2b_base64(value[len(self.prefix):]))
        return value
    def get_prep_lookup(self, lookup_type, value):
        if value and not value.startswith(self.prefix):
            encvalue = self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value)) 
            #encvalue = self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value))  
            return  encvalue

########## Charfield #################################
class AESEncryptedCharField(AESEncryptedBaseField):    
    def get_internal_type(self):
        return "CharField"
    def formfield(self, **kwargs):
        defaults = {'max_length': self.max_length}
        defaults.update(kwargs)
        return super(AESEncryptedCharField, self).formfield(**defaults)
##################### Datetime  ##############################    
class EncDatetimeFormField(CharField):
    def clean(self, value):
        from django.forms.util import ValidationError as FormValidationError
        value = super(CharField, self).clean(value)
        if value and len(value.split(' ')) != 2:
            raise FormValidationError('Date entered must be in this format dd-mm-yyyy hh:mm')    
        if value:
        	try:
        	    timeformat = datetime.strptime(value, '%d-%m-%Y %H:%M')   
        	except ValueError:  
        	 	raise FormValidationError('Date entered must be valid and in this format dd-mm-yyyy hh:mm')    
        return value


class AESEncryptedDateTimeField(AESEncryptedBaseField):  
	'''def to_python(self,value):
		if value is None:
			return value
		if value.startswith(self.prefix):
			value = enc.decrypt(settings.SECRET_KEY[:32], binascii.a2b_base64(value[len(self.prefix):]))
		if isinstance(value, datetime):
			return value
		if isinstance(value, date):
			return datetime.datetime(value.year, value.month, value.day)
		# Attempt to parse a datetime:
		value = smart_str(value)
		# split usecs, because they are not recognized by strptime.
		if '.' in value:
			try:
				value, usecs = value.split('.')
				usecs = int(usecs)
			except ValueError:
				raise exceptions.ValidationError(self.error_messages['invalid'])
		else:
			usecs = 0
		kwargs = {'microsecond': usecs}
		try: # Seconds are optional, so try converting seconds first.
			return datetime(datetime.strptime(value, '%d-%m-%Y %H:%M'),
									 **kwargs)

		except ValueError:
			try: # Try without seconds.
				return datetime(datetime.strptime(value, '%d-%m-%Y %H:%M:%S'),
										 **kwargs)
			except ValueError: # Try without hour/minutes/seconds.
				try:
					return datetime(datetime.strptime(value, '%d-%m-%Y'),
											 **kwargs)
				except ValueError:
					raise exceptions.ValidationError(self.error_messages['invalid'])'''           
  
        def get_prep_value(self, value):	
		    if value and not value.startswith(self.prefix):
			    return self.prefix + binascii.b2a_base64(enc.encrypt(settings.SECRET_KEY[:32], value)) 
			 
			   
			
        def formfield(self, **kwargs):
		defaults = {'max_length': self.max_length, 'form_class':EncDatetimeFormField}
		defaults.update(kwargs)
		return super(AESEncryptedDateTimeField, self).formfield(**defaults)



################### Text ######################    
class AESEncryptedTextField(AESEncryptedBaseField):    
    def get_internal_type(self):
        return 'TextField'
    def formfield(self, **kwargs):
            defaults = {'widget':Textarea}
            defaults.update(kwargs)
            return super(AESEncryptedTextField, self).formfield(**defaults)
        
############## Email #############
class AESEncryptedEmailField(AESEncryptedBaseField):
    from django.core import validators
    default_validators = [validators.validate_email]
    description = _("E-mail address")
    
    def get_internal_type(self):
        return "CharField"
    def formfield(self, **kwargs):
        # As with CharField, this will cause email validation to be performed twice
        defaults = {'form_class':EmailField}
        defaults.update(kwargs)
        return super(AESEncryptedEmailField, self).formfield(**defaults)
########## Integer #################
class AESEncryptedIntegerField(AESEncryptedBaseField):    
    def to_python(self, value):
        if value and value.startswith(self.prefix):
            return int(enc.decrypt(settings.SECRET_KEY[:32],binascii.a2b_base64(self.get_prep_value(value))))
        elif value and not isinstance( value, ( int, long ) ): 
            raise ValidationError('Unable to convert %s to numeric value' % value)           
                
        return int(value)
    def get_internal_type(self):
        return "CharField"
    
########## Boolean ################
    
    