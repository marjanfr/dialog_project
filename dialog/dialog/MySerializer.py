from django.core.serializers.json import Serializer
import re
import binascii

from questionnaire.models import Answers
from datetime import datetime,time,timedelta,date
from dialog.dateHelper import reformat_date
from dialog.encField import AESEncryptedBaseField,AESEncryptedIntegerField
from dialog.settings import encPrefix, SECRET_KEY
from dialog.enc import decrypt

'''class MySerializer2(Serializer):
    msg = ''
    err = ''
    type = ''
    def end_object( self, obj ):
        self._current['id'] = obj._get_pk_val()      
          
        if self.msg <> '':
            self._current['msg'] = self.msg  
        if self.type <> '':
            self._current['type'] = self.type
        self.objects.append( self._current )'''

def ValuesQuerySetToDict(vqs):
    return [item for item in vqs]

class DynamicSerializer(Serializer):
   def setExternalAtt(self, attName, attValue):
        self.addThisVal = attValue
        self.addThisName = attName
    
        
class MySerializer(DynamicSerializer):
    def find_encInteger_fields(self,obj):
        attlst = obj._meta.fields
        lstIntegerFields = []
        #find all integer fields
        for eachAttr in attlst:
            if isinstance(eachAttr, AESEncryptedIntegerField):
                lstIntegerFields.append(eachAttr.name)
        return lstIntegerFields
    
    def end_object( self, obj ):
        prefix = encPrefix
        self._current['id'] = obj._get_pk_val()  
        lstIntFields = self.find_encInteger_fields(obj)
        for key in self._current:
            if lstIntFields and key in lstIntFields:
                if isinstance(self._current[key],basestring):
                    if self._current[key].startswith(prefix):
                        self._current[key] = decrypt(SECRET_KEY[:32], binascii.a2b_base64(self._current[key][len(prefix):]))
                    self._current[key] = int(self._current[key])    
            
                       
        
        if hasattr(self,'addThisName') and hasattr(self,'addThisVal'):
            self._current[self.addThisName]=self.addThisVal   
        self.objects.append( self._current )        
        
        
class AppointmentSerializer(Serializer):
    
    embeddedChildren = None
    #answers
    def build_key_value_pair(self, parent, lstKeys):     
        values =  self._current[parent]
        if len(values) <> len(lstKeys):
            return 'indexOutOfRange'
        #values[0] = values[0][1:]  
        dict = {}
        for i in range(len(values)):
            dict[lstKeys[i]] = values[i]
        self._current[parent] = dict 
    def extra_embeddedChildren(self,title, childrenList):
        self.embeddedChildren = childrenList
        self.embeddedChildren_title = title   
        
    def convert_time_integer(self, timeValue):
        minutes, seconds = map(int, timeValue.split(':'))
        sum= seconds + minutes*60
        return sum           
    def list_of_dictionnairy_for_fixed_key(self, key,lstValue):   
        lstOfDic = []
        for currVal in lstValue:
            lstOfDic.append('') 
        return lstOfDic
         
    def end_object( self, obj ):
        self._current['id'] = obj._get_pk_val() 
        if self.embeddedChildren and self._current['id'] in self.embeddedChildren:
            self._current[self.embeddedChildren_title] = self.embeddedChildren[self._current['id']]
        if 'startDate' in self._current and self._current['startDate']:
            self._current['startDate'] = reformat_date(self._current['startDate'])
        if 'endDate' in self._current and self._current['endDate']:
            self._current['endDate'] = reformat_date(self._current['endDate'])    
        if 'duration' in self._current and self._current['duration']:
           self._current['duration'] = self.convert_time_integer(self._current['duration'])
        if 'seenTime' in self._current and self._current['seenTime']:
           self._current['seenTime'] = '1997-01-01T'+ str(self._current['seenTime']) 
        if 'participant' in self._current and self._current['participant']:
            self._current['participant'] = self.list_of_dictionnairy_for_fixed_key("name",self._current['participant'].split(","))
        self.objects.append( self._current )
    

'''class AppointmentSerializer2(Serializer):
    msg = ''
    err = ''
    type = ''
    
    def build_key_value_pair(self, parent, lstKeys):     
        values =  self._current[parent]
        if len(values) <> len(lstKeys):
            return 'indexOutOfRange'
        #values[0] = values[0][1:]  
        dict = {}
        for i in range(len(values)):
            dict[lstKeys[i]] = values[i]
        self._current[parent] = dict   
         
    def end_object( self, obj ):
        self._current['id'] = obj._get_pk_val() 
        lstKeys = ['nickname','address_ln1','address_ln3','city', 'postcode','telHome','telMobile','telWork','telOther','email']
        self.build_key_value_pair('patient', lstKeys) 
        lstKeysCC = ['ccId','firstName','lastName','username']         
        self.build_key_value_pair('cc', lstKeysCC)  
        if self.msg <> '':
            self._current['msg'] = self.msg  
        if self.type <> '':
            self._current['type'] = self.type       
        self.objects.append( self._current )  '''  

        



