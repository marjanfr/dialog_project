#!/usr/bin/env python
import string
from random import choice
from Crypto.Cipher import AES

EOD = '`%EofD%`' # This should be something that will not occur in strings

def genstring(length=16, chars=string.printable):
    charlist = ['a','b','c','d','e','f','j','g','h','i','k','l','n','m','o','p','q']
    genstr = ''
    for i in range(length):
        genstr += charlist[i]
    return genstr

def encryptInteger(key,s):
    return s ^ key

    
def decrypt1(key,s):
    #key = '0123456789abcdef'
   # mode = AES.MODE_CBC
    decryptor = AES.new(key)
    plain = decryptor.decrypt(s)
    return plain
    
def encrypt(key, s):
    mode = AES.MODE_CBC
    obj = AES.new(key,mode)
    datalength = len(s) + len(EOD)
    if datalength < 16:
        saltlength = 16 - datalength
    else:
        saltlength = 16 - datalength % 16
    ss = ''.join([s, EOD, genstring(saltlength)])
    return obj.encrypt(ss)

def decrypt(key, s):
    mode = AES.MODE_CBC
    obj = AES.new(key,mode)
    ss = obj.decrypt(s)
    return ss.split(EOD)[0]

if __name__ == '__main__':
    for i in xrange(8, 20):
        s = genstring(i)
        key = genstring(32)
        print 'The key is', key
        print 'The string is', s, i
        cipher  = encrypt(key, s)
        print 'The encrypted string is', cipher
        print 'This decrypted string is', decrypt(key, cipher)