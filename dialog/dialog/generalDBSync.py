from django import http
from django.utils import simplejson as json
from django.utils.timezone import datetime, utc
from django.contrib.auth.hashers import check_password
#from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
#from django.db.models import ForeignKey
#from uuid import uuid4

#from django.core.context_processors import request
from session.models import CareCoordinators, Patients, Appointments
from questionnaire.models import Answers, ActionItem
#from httplib import HTTP
#from django.core import serializers
#from django.contrib.auth import authenticate
#from dialog.base import *
from dialog.MySerializer import MySerializer, AppointmentSerializer,AppointmentSerializer
from authentication.views import check_user_key
from dialog.MyStr import clean_json, get_dic_output
from dialog.Error import json_response_message
from django.db.models import Q
from dialog.enc import decrypt
from dialog.encField import AESEncryptedIntegerField
import binascii

from settings import encPrefix, SECRET_KEY


def get_model_fields(model):
    return model._meta.fields

def find_encInteger_fields(model):
    attlst = get_model_fields(model)
    lstIntegerFields = []
    #find all integer fields
    for eachAttr in attlst:
        if isinstance(eachAttr, AESEncryptedIntegerField):
            lstIntegerFields.append(eachAttr.name)
    return lstIntegerFields
    
             
    
def decryptQueryList(model,lstChild):
    prefix = encPrefix
    lstIntFields = find_encInteger_fields(model)
    for eachItem in lstChild:
        for key in eachItem.keys():
            if isinstance(eachItem[key],basestring) and eachItem[key].startswith(prefix) :
                    eachItem[key] = decrypt(SECRET_KEY[:32], binascii.a2b_base64(eachItem[key][len(prefix):]))
                    if lstIntFields and key in lstIntFields:
                        eachItem[key] = int(eachItem[key])
    return lstChild

def generate_parent_child(parentField, rawChildren, addEmbeddedChildren=False,embeddedChildrenTitle = '', embeddedChildren=None):    
    # this method generates a dic which its keys are parentId and its values are json array of rawChildren
    dicParentChild = {}
    lstChild = []
    thisParentId = None     
    for resultDic in rawChildren:
        parentId = resultDic[parentField]
        if parentId <> thisParentId:
            if thisParentId:                    
                #it's not a first time
                dicParentChild[thisParentId]=  clean_json(json.dumps(lstChild)) 
                del lstChild[:]
            thisParentId = parentId 
            
        del resultDic[parentField]
        if addEmbeddedChildren and resultDic['id'] in embeddedChildren:
            resultDic[embeddedChildrenTitle] = embeddedChildren[resultDic['id']]
        lstChild.append(resultDic)
    dicParentChild[thisParentId]=  clean_json(json.dumps(lstChild)) 
    return dicParentChild

'''def convert_a_model_to_json(model,has_reverse_relation=False, reverse_model=None,reverse_model_name=None,ser=False):
     modelDic = {}
     attlst = get_model_fields(model)
     ms = MySerializer()
     for eachAttr in attlst:
         if not isinstance(eachAttr, ForeignKey):
             modelDic[eachAttr.name] = getattr(model,eachAttr.name)
         elif ser:
             modelDic[eachAttr.name] = ms.serialize({getattr(model,eachAttr.name)})
             
     if has_reverse_relation:
         modelDic[reverse_model_name]=reverse_model
                      
     data = clean_json(json.dumps(modelDic))
     return data'''
 
'''def grab_model_reverse_related_model(modelresultList,parentName):
           
        #parentName = "answer_id"
    dicParentChild = {}
    thisParentId = None
    for modelResult in modelresultList:      
        parentId = getattr(modelResult,parentName)
        if thisParentId == parentId:
                dicParentChild[str(parentId)]+= convert_a_model_to_json(modelResult)
        else:
                thisParentId = parentId
                #dicParentChild[str(parentId)] = None
                dicParentChild[str(parentId)]= convert_a_model_to_json(modelResult)
        return dicParentChild'''


@csrf_exempt
@check_user_key('500')
def get_all_viaClient(request):
    #methodType='all_viaclient'
    methodType='500'
    if request.method == 'POST':
        passedcId = int(request.POST.get('cId', -1))
    '''else:
        return json_response_message("-50",methodType)''' 
    
    dic = {}
    #passedcId=1
    if passedcId == -1:
        return json_response_message("-210",methodType, parent='appointments')  
    else:             
        
        appointments = Appointments.objects.select_related('outcome').filter(patient=passedcId)            
        answers2 = Answers.objects.filter(patient=passedcId).values('id','question','patient','appointment','ranklable','needHelp','isSelected')
        answers2 = decryptQueryList(answers2)
        actions2 = ActionItem.objects.filter(answer__patient = passedcId).values('title','answer','order')
        actions2 = decryptQueryList(actions2)
        
        
        answer_actions = generate_parent_child('answer', actions2)
        appointment_answers = generate_parent_child('appointment', answers2, True, 'action', answer_actions)
 
        appointmentSerialiser = AppointmentSerializer()   
        appointmentSerialiser.extra_embeddedChildren('answer', appointment_answers)       
        data = appointmentSerialiser.serialize(appointments,use_natural_keys=False)         
    #{"msg": "True", "output": [{"startDate": "2012-09-17T21:04:15Z", "endDate": "2012-09-17T21:04:16Z", "cc": 1, "seenTime": "1997-01-01T16:04:16", "comments": "", "answer": [{"action": [{"title": "call him again"}, {"title": "sport activities"}], "patient": 1, "question": 1, "id": 1, "ranklable": 1}, {"action": [{"title": "blood test result"}], "patient": 1, "question": 2, "id": 2, "ranklable": 3}], "duration": null, "patient": 1, "outcome": 16, "id": 1}], "type": "all_viaclient", "err": null}
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('1',methodType,data))), mimetype='application/javascript')


#csrf_exempt
#@check_user_key('all_viaclient')
'''def get_all_viaClient3(request):
    methodType='all_viaclient'
    if request.method == 'POST':
        passedcId = int(request.POST.get('cId', -1))
    else:
        return http.HttpResponse(json.dumps({"msg":"False", "err":"PostRequestNotFound"}), mimetype='application/javascript')
    
    dic = {}
    #passedcId=1
    if passedcId == -1:
        return json_response_message("cIdNotFound",methodType, parent='appointments')  
    else:        
       
        #appointments = Appointments.objects.select_related().filter(patient=passedcId).prefetch_related('answers')
        appointments = Appointments.objects.select_related().filter(patient=passedcId)               
        answers = Answers.objects.filter(patient=passedcId)
        answers2 = Answers.objects.filter(patient=passedcId).values('question','patient','appointment','ranklable')
       
        
        
        actions = ActionItem.objects.filter(answer__patient = passedcId).order_by('answer') 
        actions2 = ActionItem.objects.filter(answer__patient = passedcId).values('title','answer')
        
        mySerialiser = MySerializer()
        appserialized = mySerialiser.serialize(appointments,use_natural_keys=True) 
        
        # ValuesQuerySet: [{'appointment': 1, 'patient': 1, 'question': 1, 'ranklable': 1}]
        # ValuesQuerySet: [{'answer': 1, 'title': u'call him again'}, {'answer': 1, 'title': u'sport activities'}]
        
        modelresultList = actions
        parentName = "answer_id"
        dicParentChild = {}
        thisParentId = None
        for modelResult in modelresultList:            
            parentId = getattr(modelResult,parentName)
            if thisParentId == parentId:
                dicParentChild[str(parentId)] = convert_a_model_to_json(modelResult) + ',' + dicParentChild[str(parentId)]
            else:
                thisParentId = parentId
                #dicParentChild[str(parentId)] = None
                dicParentChild[str(parentId)]= convert_a_model_to_json(modelResult)
                
        modelresultList = answers
        parentName = "appointment_id" 
        dicParentChild2 = {}
        thisParentId = None
        for modelResult in modelresultList:            
            parentId = getattr(modelResult,parentName)
            if thisParentId == parentId:
                dicParentChild2[str(parentId)]= convert_a_model_to_json(modelResult,True, dicParentChild[str(modelResult.id)],'action') + ','+dicParentChild2[str(parentId)]
            else:
                thisParentId = parentId
                #dicParentChild[str(parentId)] = None
                dicParentChild2[str(parentId)]= convert_a_model_to_json(modelResult,True, dicParentChild[str(modelResult.id)],'action',True)
            
        
        
        ''''''modelDic = {}
        attlst = get_model_fields(Appointments)
        modelresultList = appointments
        for model in modelresultList:
           for eachAttr in attlst:               
                   modelDic[eachAttr.name] = getattr(model,eachAttr.name)''''''
             
        appointmentSerialiser = AppointmentSerializer()   
        #appointmentSerialiser.msg = "True"  
        #appointmentSerialiser.type = methodType
        
        dic['appointments'] = appointmentSerialiser.serialize(appointments,use_natural_keys=True) 
        mySerialiser = MySerializer()
        #dic['answers'] = mySerialiser.serialize(answers)
        ''''''dic['actions'] = mySerialiser.serialize(actions)''''''
        
    #{"msg": "True", "output": {"appointments": [{"startDate": "2012-09-06T11:00:00Z", "patient": {"city": "london", "telHome": "", "email": "", "telMobile": "", "postcode": "ub4", "telWork": "", "telOther": "", "nickname": "mns", "address_ln1": "136 wiloo", "address_ln2": "", "address_ln3": ""}, "title": "test app", "cc": {"lastName": "Falah rastegar", "ccId": 1, "firstName": "Marjan"}, "comments": "", "endDate": "2012-09-13T17:00:00Z", "outcome": null, "id": 2}, {"startDate": "2012-09-06T02:51:08Z", "patient": {"city": "london", "telHome": "", "email": "", "telMobile": "", "postcode": "ub4", "telWork": "", "telOther": "", "nickname": "mns", "address_ln1": "136 wiloo", "address_ln2": "", "address_ln3": ""}, "title": "test", "cc": {"lastName": "Falah rastegar", "ccId": 1, "firstName": "Marjan"}, "comments": "", "endDate": "2012-09-11T11:00:00Z", "outcome": null, "id": 1}], "actions": [], "answers": [{"appointment": 1, "patient": 1, "question": 1, "id": 1, "ranklable": 22}, {"appointment": 1, "patient": 1, "question": 1, "id": 2, "ranklable": 44}]}, "type": "all_viaclient", "err": null}
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('True',methodType,dic))), mimetype='application/javascript')'''



'''@csrf_exempt
@check_user_key('all_viaclient')
def get_all_viaClient1(request):
    methodType='all_viaclient'
    if request.method == 'POST':
        passedcId = int(request.POST.get('cId', -1))
    else:
        return http.HttpResponse(json.dumps({"msg":"False", "err":"PostRequestNotFound"}), mimetype='application/javascript')
    
    dic = {}
    #passedcId=1
    if passedcId == -1:
        return json_response_message("cIdNotFound",methodType, parent='appointments')  
    else:        
        appointments = Appointments.objects.select_related().filter(patient=passedcId).prefetch_related('answers')
        #appointments = Appointments.objects.select_related().filter(patient=passedcId)               
        answers = Answers.objects.filter(patient=passedcId)
        actions = ActionItem.objects.filter(answer__patient = passedcId)  
        appointmentSerialiser = AppointmentSerializer()   
        #appointmentSerialiser.msg = "True"  
        #appointmentSerialiser.type = methodType
        
        dic['appointments'] = appointmentSerialiser.serialize(appointments,use_natural_keys=True) 
        mySerialiser = MySerializer()
        #dic['answers'] = mySerialiser.serialize(answers)
        ''''''dic['actions'] = mySerialiser.serialize(actions)''''''
        
    #{"msg": "True", "output": {"appointments": [{"startDate": "2012-09-06T11:00:00Z", "patient": {"city": "london", "telHome": "", "email": "", "telMobile": "", "postcode": "ub4", "telWork": "", "telOther": "", "nickname": "mns", "address_ln1": "136 wiloo", "address_ln2": "", "address_ln3": ""}, "title": "test app", "cc": {"lastName": "Falah rastegar", "ccId": 1, "firstName": "Marjan"}, "comments": "", "endDate": "2012-09-13T17:00:00Z", "outcome": null, "id": 2}, {"startDate": "2012-09-06T02:51:08Z", "patient": {"city": "london", "telHome": "", "email": "", "telMobile": "", "postcode": "ub4", "telWork": "", "telOther": "", "nickname": "mns", "address_ln1": "136 wiloo", "address_ln2": "", "address_ln3": ""}, "title": "test", "cc": {"lastName": "Falah rastegar", "ccId": 1, "firstName": "Marjan"}, "comments": "", "endDate": "2012-09-11T11:00:00Z", "outcome": null, "id": 1}], "actions": [], "answers": [{"appointment": 1, "patient": 1, "question": 1, "id": 1, "ranklable": 22}, {"appointment": 1, "patient": 1, "question": 1, "id": 2, "ranklable": 44}]}, "type": "all_viaclient", "err": null}
    return http.HttpResponse(clean_json(json.dumps(get_dic_output('True',methodType,dic))), mimetype='application/javascript')
'''
@csrf_exempt
@check_user_key('510')
def get_all_viaAppointment(request):
    #methodType = 'all_viaappt'         
    methodType = '510'           
    if request.method == 'POST':
        passedaptId = int(request.POST.get('aptId', -1))
    else:
        return json_response_message("-50",methodType) 
    
    dic = {}
    #passedaptId=1
    if passedaptId == -1:
        return json_response_message("-400",methodType,parent="appointments")
    else:        
        appointments = Appointments.objects.select_related().filter(id=passedaptId)       
        answers = Answers.objects.filter(appointment=passedaptId).values('id','question','patient','appointment','ranklable','needHelp','isSelected')
        answers = decryptQueryList(Answers,answers)
        actions = ActionItem.objects.filter(answer__appointment = passedaptId).values('title','answer','order')
        actions = decryptQueryList(ActionItem,actions)
        #for eachAction in actions:
        #actions[1]['order'] =  (int(actions[1]['order']))
        answer_actions = generate_parent_child('answer', actions)
        appointment_answers = generate_parent_child('appointment', answers, True, 'action', answer_actions)
        
        appointmentSerialiser = AppointmentSerializer()   
        appointmentSerialiser.extra_embeddedChildren('answer', appointment_answers)       
        data = appointmentSerialiser.serialize(appointments,use_natural_keys=False)     
        
        hh = clean_json(json.dumps(get_dic_output('1',methodType,data)))
       
    return http.HttpResponse(hh, mimetype='application/javascript')

@csrf_exempt
@check_user_key('all_viadate')
def get_all_viaDate(request):
    #if request.method == 'POST':
    passedStartDate = int(request.POST.get('startDate', -1))
    passedEndDate = int(request.POST.get('endDate', -1))
    #else:
     #   return http.HttpResponse('null')
    
    
    #passedaptId=1
    passedStartDate = '2012-01-01'
    passedEndDate = '2012-12-30'
    dic = {}
    if passedStartDate == -1 or passedEndDate == -1 :
        data = {"error":"cId not found"}  
    else:        
        appointments = Appointments.objects.select_related().filter( Q(startDate__gte= passedStartDate), Q(startDate__lte= passedEndDate), Q(endDate__gte= passedStartDate),Q(endDate__lte= passedEndDate) )
        
        answers = []
        actions = []
        if appointments.exists():   
            for appointment in appointments:
                answer = Answers.objects.filter(appointment=appointment.id)
                answers.append( answer)
                #action = ActionItem.objects.filter(answer__appointment = appointments.id)
                #actions.append( action)  
            mySerialiser = MySerializer()       
            dic['appointments'] = mySerialiser.serialize(appointments) 
            dic['answers'] = mySerialiser.serialize(answers)
            #dic['actions'] = mySerialiser.serialize(actions)
    
        else:
            dic={}
    #output: "[{\"startDate\": \"2012-08-10T21:10:57Z\", \"endDate\": \"2012-08-15T05:00:00Z\", \"id\": 1, \"title\": \"test\"}]"
    return http.HttpResponse(clean_json(json.dumps(dic)), mimetype='application/javascript')
