from django import http
from django.utils import simplejson as json

from MyStr import get_dic_output

def json_response_message(errMsg, type,isValid="0", parent=None):
    if parent is None:
        return http.HttpResponse(json.dumps(get_dic_output(isValid,type,None,errMsg)), mimetype='application/javascript')
    else:
        return http.HttpResponse(json.dumps({parent:[{"msg":isValid, "err":errMsg, "type":type}]}), mimetype='application/javascript')
    